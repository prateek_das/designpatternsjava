package creational.factory.abstractFactory.factory;

import creational.factory.abstractFactory.model.Shape;
import creational.factory.abstractFactory.model.Square;

public class SquareFactory extends AbstractShapeFactory{
    @Override
    protected Shape factoryMethod() {
        return new Square();
    }
}
