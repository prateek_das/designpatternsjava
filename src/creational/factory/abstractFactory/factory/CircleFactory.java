
package creational.factory.abstractFactory.factory;

import creational.factory.abstractFactory.model.Circle;
import creational.factory.abstractFactory.model.Shape;

public class CircleFactory extends AbstractShapeFactory{
    @Override
    protected Shape factoryMethod(){
        return new Circle();
    }
}
