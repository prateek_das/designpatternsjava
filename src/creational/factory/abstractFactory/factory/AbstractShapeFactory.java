package creational.factory.abstractFactory.factory;

import creational.factory.abstractFactory.model.Shape;

public abstract class AbstractShapeFactory {
    protected abstract Shape factoryMethod();

    public Shape getShape(){
        return factoryMethod();
    }
}
