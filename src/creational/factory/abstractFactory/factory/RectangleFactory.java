package creational.factory.abstractFactory.factory;

import creational.factory.abstractFactory.model.Rectangle;
import creational.factory.abstractFactory.model.Shape;

public class RectangleFactory extends AbstractShapeFactory{
    @Override
    protected Shape factoryMethod(){
        return new Rectangle();
    }
}
