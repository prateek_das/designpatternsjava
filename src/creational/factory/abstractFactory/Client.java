package creational.factory.abstractFactory;

import creational.factory.abstractFactory.factory.CircleFactory;
import creational.factory.abstractFactory.factory.RectangleFactory;
import creational.factory.abstractFactory.factory.SquareFactory;
import creational.factory.abstractFactory.model.Shape;

public class Client {
    public static void main(String[] args) {
        Shape circle = new CircleFactory().getShape();
        circle.draw();

        Shape rectangle = new RectangleFactory().getShape();
        rectangle.draw();

        Shape square = new SquareFactory().getShape();
        square.draw();
    }
}
