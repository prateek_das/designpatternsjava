package creational.factory.abstractFactory.model;

public class Circle implements Shape{
    @Override
    public void draw() {
        System.out.println("This is circle.");
    }
}
