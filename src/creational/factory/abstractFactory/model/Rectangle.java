package creational.factory.abstractFactory.model;

public class Rectangle implements Shape{
    @Override
    public void draw() {
        System.out.println("This is rectangle");
    }
}
