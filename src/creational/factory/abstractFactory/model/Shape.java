package creational.factory.abstractFactory.model;
public interface Shape {

    void draw();
}