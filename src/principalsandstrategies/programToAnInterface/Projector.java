package principalsandstrategies.programToAnInterface;

public class Projector implements DisplayModule {
    public void display(){
        System.out.println("Display through projector");
    }
}
