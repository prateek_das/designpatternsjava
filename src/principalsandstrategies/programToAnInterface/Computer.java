package principalsandstrategies.programToAnInterface;

public class Computer {
    DisplayModule dm;

    public void setDisplayModule(DisplayModule dm){
        this.dm = dm;
    }

    public void display()
    {
        dm.display();
    }

    public static void main(String[] args) {
        //Programming to an interface using polymorphism
        Computer cm = new Computer();
        DisplayModule dm = new Monitor();
        DisplayModule dm1 = new Projector();

        cm.setDisplayModule(dm);
        cm.display();
        cm.setDisplayModule(dm1);
        cm.display();
    }
}
