package principalsandstrategies.programToAnInterface;

interface DisplayModule {

    public void display();
}
