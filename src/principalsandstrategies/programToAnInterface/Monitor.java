package principalsandstrategies.programToAnInterface;

public class Monitor implements DisplayModule {

    public void display() {
        System.out.println("Display through Monitor");
    }
}
