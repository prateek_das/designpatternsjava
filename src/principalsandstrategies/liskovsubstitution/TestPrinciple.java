package principalsandstrategies.liskovsubstitution;

public class TestPrinciple {

    public static void main(String[] args) {
        Vehicle vehicle = new Bus();
        vehicle.getSpeed();

        // Subtype objects can replace supertype objects in Object reference
        vehicle = new Car();
        vehicle.getCubicCapacity();
    }
}
