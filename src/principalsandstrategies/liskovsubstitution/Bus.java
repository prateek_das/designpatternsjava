package principalsandstrategies.liskovsubstitution;

public class Bus extends Vehicle{
    @Override
    int getSpeed() {
        return 0;
    }

    @Override
    int getCubicCapacity() {
        return 0;
    }

    String getEmergencyExitLoc(){
        return "";
    }
}
