package principalsandstrategies.liskovsubstitution;

public abstract class Vehicle {
    abstract int getSpeed();
    abstract int getCubicCapacity();

}
