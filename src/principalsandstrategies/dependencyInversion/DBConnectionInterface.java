package principalsandstrategies.dependencyInversion;

public interface DBConnectionInterface {
    public int connect();
}
