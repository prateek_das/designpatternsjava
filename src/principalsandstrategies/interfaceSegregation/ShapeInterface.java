package principalsandstrategies.interfaceSegregation;

public interface ShapeInterface {
    public double area();
}
