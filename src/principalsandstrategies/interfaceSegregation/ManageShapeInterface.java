package principalsandstrategies.interfaceSegregation;

public interface ManageShapeInterface {
    public double calculate();
}
