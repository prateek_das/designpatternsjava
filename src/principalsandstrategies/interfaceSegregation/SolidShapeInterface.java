package principalsandstrategies.interfaceSegregation;

public interface SolidShapeInterface {
    public double volume();
}
