package principalsandstrategies.openclose;

public interface Shape {
    public double calculateArea();
}
