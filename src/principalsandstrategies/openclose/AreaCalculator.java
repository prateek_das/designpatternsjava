package principalsandstrategies.openclose;

public class AreaCalculator {
    /*
    commenting. To change it to programming to an interface and delegation

    public double calculateRectangleArea(Rectangle rec){
        return rec.length * rec.width;
    }

    public double calculateCircleArea(Circle c){
        return c.radius * c.radius * (22/7);
    }*/

    // This design is open for extension and closed for modification.
    // We just need to implement shape and calculateArea method for a new shape.
    // AreaCalculator doesn't need modification for that.
    // It is polymorphic because it takes Shape
    public double calculateShapeArea(Shape shape){
        //Delegation: assigning responsibility for calculating area to various shape classes
        return shape.calculateArea();
    }
}
