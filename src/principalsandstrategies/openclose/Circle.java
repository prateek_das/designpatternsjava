package principalsandstrategies.openclose;

public class Circle implements Shape{
    public double radius;

    @Override
    public double calculateArea() {
        return radius * radius * (22/7);
    }
}
