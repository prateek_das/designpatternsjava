package principalsandstrategies.delegation;

// the "delegate"
public class RealPrinter {

    void print(){
        System.out.println("The Delegate");
    }
}
