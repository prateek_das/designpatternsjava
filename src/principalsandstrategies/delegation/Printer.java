package principalsandstrategies.delegation;

public class Printer {
    // the "delegator"

    RealPrinter p = new RealPrinter();

    //crate the delegate
    void print(){
        p.print(); //delegate
    }
}
