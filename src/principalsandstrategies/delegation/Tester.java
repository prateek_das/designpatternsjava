package principalsandstrategies.delegation;

public class Tester {
    public static void main(String[] args) {
        // To outside world, seems the printer actually prints
        Printer p = new Printer();
        p.print();
    }
}
